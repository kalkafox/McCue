package com.zeitheron.mccue.client.sdk.corsair.icue;

public enum DeviceCaps
{
	CDC_None,
	CDC_Lighting,
	CDC_PropertyLookup
}