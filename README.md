This is not an official repository. Visit the [CurseForge](https://www.curseforge.com/minecraft/mc-mods/corsair-mccue) link or [source code.](https://gitlab.com/DragonForge/McCue)

All rights go to [Zeitheron](https://www.curseforge.com/members/zeitheron/projects)